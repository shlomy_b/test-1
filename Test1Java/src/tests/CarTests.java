// Shlomo Bensimhon	
// 1837702

package tests;

import vehicles.Car;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class CarTests {

	@Test
	public void carConstructorTest() {
		assertEquals(150, new Car(150).getSpeed());
		assertEquals(1, new Car(1).getSpeed());
		assertEquals(0, new Car(0).getSpeed());
		try{
			 new Car(-1);
			 	fail("Car Constructor Didnt Throw An Exception");
		}
		catch (IllegalArgumentException e) {
		
		}
		catch (Exception e) {
			fail("Car Constructor Did Throw An Exception But it Wasnt The Right Excpection");
		}
	}
	
	@Test
	public void getSpeedTest() {
		assertEquals(150, new Car(150).getSpeed());
		assertEquals(0, new Car(0).getSpeed());
		assertEquals(10000, new Car(10000).getSpeed());
	}
	
	@Test
	public void getLocationTest() {
		Car c1 = new Car(150);
		assertEquals(0, c1.getLocation());
		c1.moveRight();
		assertEquals(150, c1.getLocation());
		c1.moveLeft();
		assertEquals(0, c1.getLocation());
		c1.moveLeft();
		assertEquals(-150, c1.getLocation());
	}
	
	@Test
	public void moveRightTest() {
		Car c2 = new Car(100);
		assertEquals(0, c2.getLocation());
		c2.moveRight();
		assertEquals(100, c2.getLocation());
		c2.moveRight();
		assertEquals(200, c2.getLocation());
		c2.moveRight();
		assertEquals(300, c2.getLocation());
	}
	
	@Test
	public void moveLeftTest() {
		Car c3 = new Car(100);
		assertEquals(0, c3.getLocation());
		c3.moveLeft();
		assertEquals(-100, c3.getLocation());
		c3.moveLeft();
		assertEquals(-200, c3.getLocation());
		c3.moveLeft();
		assertEquals(-300, c3.getLocation());
	}
	
	@Test
	public void accelerateTest() {
		Car c4 = new Car(180);
		assertEquals(180, c4.getSpeed());
		c4.accelerate();
		assertEquals(181, c4.getSpeed());
		c4.accelerate();
		assertEquals(182, c4.getSpeed());
	}
	
	@Test
	public void decelerateTest() {
		Car c5 = new Car(0);
		c5.decelerate();
		assertEquals(0, c5.getSpeed());
		c5.accelerate();
		c5.accelerate();
		c5.accelerate();
		c5.decelerate();
		assertEquals(2, c5.getSpeed());
		c5.decelerate();
		assertEquals(1, c5.getSpeed());
	}
}

	
	
	
	
	
	
	
	
	
	
	
	