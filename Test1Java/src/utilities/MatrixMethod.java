package utilities;

public class MatrixMethod {
	 public static int[][] duplicate(int[][] array){
		  
		  int[][] tempArr = new int[array.length][(array[0].length * 2)];  

		  for (int i = 0; i < tempArr.length ; i++) {
			  int counter = 0;
		   for(int j = 0; j < tempArr[i].length; j++) {
		    if(j < array[0].length) {
		      tempArr[i][j] = array[i][j];
		    }else {
		     tempArr[i][j] = array[i][counter];
		     counter++;
		    }
		   }
		  }
		  return tempArr;
		 }
}
		